<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCableServiceChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cable_service_channel', function (Blueprint $table) {
            $table->unsignedInteger('cable_service_id');
            $table->unsignedInteger('channel_id');
            $table->primary(['cable_service_id', 'channel_id']);
            $table->foreign('cable_service_id')
                    ->references('id')
                    ->on('cable_services');
            $table->foreign('channel_id')
                    ->references('id')
                    ->on('channels');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cable_service_channel');
    }
}
