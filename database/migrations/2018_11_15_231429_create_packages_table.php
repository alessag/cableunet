<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');

            $table->unsignedInteger('internet_service_id')->nullable();
            $table->unsignedInteger('cable_service_id')->nullable();
            $table->unsignedInteger('phone_service_id')->nullable();

            $table->foreign('phone_service_id')
                    ->references('id')
                    ->on('phone_services');

            $table->foreign('cable_service_id')
                    ->references('id')
                    ->on('cable_services');

            $table->foreign('internet_service_id')
                    ->references('id')
                    ->on('internet_services');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
