<?php

use Illuminate\Database\Seeder;

use App\Role;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /**
         * Create the Roles
         * 0 - Admin
         * 1 - Subs
         */
        $admin = Role::create([
            'id' => '0',
            'title' => 'Admin',
            'description' => 'Can create and edit services.' 
        ]);

        $sub = Role::create([
            'id' => '1',
            'title' => 'Subscriber',
            'description' => 'Use the services and get invoices.' 
        ]);
        
        /**
         * Create the default admin
         * Email = admin@admin.com
         * Password = Secret
         */
        $user = factory(App\User::class)->create([
            'email' => 'admin@admin.com'
        ]);

        //Gives the user the role admin
        $user->role()->associate($admin);
        $user->save();
    }
}
