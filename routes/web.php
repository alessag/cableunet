<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('phone', 'PhoneServiceController');
Route::resource('internet', 'InternetServiceController');
Route::resource('cable', 'CableServiceController');
Route::resource('channel', 'ChannelController');
Route::resource('package', 'PackageController');

Route::post('package/add','AdminController@addPackage')->name('package.add');
Route::get('users', 'AdminController@indexUsers')->name('user.index');
Route::get('user/{user}', 'AdminController@showUser')->name('user.show');
Route::post('user/approved', 'AdminController@approvePackage')->name('user.ok');

Route::get('invoices', 'InvoiceController@index')->name('invoice.index');
Route::get('invoice/{invoice}', 'InvoiceController@show')->name('invoice.show');

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
