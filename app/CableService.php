<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Channel;
use App\Package;

class CableService extends Model
{
    /**
     * @var array
     */
    protected $guarded =[];

    public function channels(){
        return $this->belongsToMany(Channel::class);
    }

    public function packages(){
        return $this->hasMany(Package::class);
    }
}
