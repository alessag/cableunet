<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Role extends Model
{
    /**
     * @var array
     * 
     */
    protected $guarded = [];

    /**
     * The users associate with this role
     * 
     * @return App\User
     */
    public function users(){
        return $this->hasMany(User::class);
    }
}
