<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Package;
use App\User;
use App\Invoice;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->except(['addPackage']);
    }

    public function addPackage(Request $request){
        $package = Package::find($request->get('package_id'));
        if( is_null($package) ) return redirect()->back()->withErrors('El paquete seleccionado no existe, intentelo de nuevo!');
        $user = auth()->user();
        $user->package()->associate($package);
        $user->pendingPackage();
        $user->save();
        return redirect()->route('home')->withSuccess('Su solicitud esta siendo procesada!');
    }

    public function approvePackage(Request $request){
        $user = User::find($request->get('user_id'));
        if( is_null($user) ) return redirect()->back()->withErrors('El Usuario seleccionado no existe, intentelo de nuevo!');
        $user->approvePackage();
        $user->save();
        return redirect()->back()->withSuccess('Solicitud aprobada con exito!');
    }

    public function indexUsers(){
        $users = User::where('role_id', 1)->get();
        return view('admin.users', compact('users'));
    }

    public function showUser( User $user ){
        if( $user->isAdmin() ) return redirect()->route('home')->withErrors('No puedes hacer eso!');
        return view('admin.showUser', compact('user'));
    }
}
