<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invoice;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Invoice $invoice){
        $invoice->loadMissing('user');
        return view('invoice.show', compact('invoice'));
    }

    public function index(){
        if ( auth()->user()->isAdmin() ) 
        $invoices = Invoice::all();
        else
        $invoices = auth()->user()->invoices->loadMissing('user');
        return view('invoice.index', compact('invoices'));
    }
}
