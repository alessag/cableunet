<?php

namespace App\Http\Controllers;

use App\InternetService;
use Illuminate\Http\Request;

class InternetServiceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['show', 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $internets = InternetService::all();
        return view('services.internet.index', compact('internets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.internet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'price' => 'required|numeric|min:1',
            'speed' => 'required|numeric|min:1',
        ]);

        $internet = InternetService::create($validated);
        return redirect()->route('internet.show' , $internet)->withSuccess('Servicio creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InternetService  $internet
     * @return \Illuminate\Http\Response
     */
    public function show(InternetService $internet)
    {
        return view('services.internet.show',compact('internet'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InternetService  $internet
     * @return \Illuminate\Http\Response
     */
    public function edit(InternetService $internet)
    {
        return view('services.internet.edit', compact('internet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InternetService  $internet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InternetService $internet)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'price' => 'required|numeric|min:1',
            'minutes' => 'required|numeric|min:1',
        ]);

        $internet->fill($validated);
        $internet->save();
        return redirect()->route('internet.show' , $internet)->withSuccess('Servicio editado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InternetService  $internet
     * @return \Illuminate\Http\Response
     */
    public function destroy(InternetService $internet)
    {
        $internet->delete();
        return redirect()->route('home')->withSuccess('Servicio borrado con exito');
    }
}
