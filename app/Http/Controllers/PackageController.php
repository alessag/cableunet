<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;

use App\CableService as Cable;
use App\InternetService as Internet;
use App\PhoneService as Phone;

class PackageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['show', 'index']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();
        return view('services.package.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cables = Cable::with('channels')->get();
        $internets = Internet::all();
        $phones = Phone::all();

        return view('services.package.create' , compact('phones' , 'cables' , 'internets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'phone' => 'nullable',
            'cable' => 'nullable',
            'internet' => 'nullable',
        ]);
        if( $validated['phone'] == 0 and $validated['cable']  == 0 and $validated['internet'] == 0) return redirect()->back()->withErrors('El paquete tiene que tener al menos un servicio!');

        $package = Package::create( array_only($validated, ['title', 'description'] ) );

        if( array_has($validated , 'internet' ) and $validated['internet'] != 0  ){
            $internet = Internet::find( $validated['internet'] );
            if( is_null( $internet ) ) return redirect()->back()->withErrors('El servicio de internet seleccionado no existe, por favor intentelo de nuevo');
            $package->internet()->associate( $internet );
        }

        if( array_has($validated , 'cable' ) and $validated['cable'] != 0  ){
            $cable = Cable::find( $validated['cable'] );
            if( is_null( $cable ) ) return redirect()->back()->withErrors('El servicio de cable seleccionado no existe, por favor intentelo de nuevo');
            $package->cable()->associate( $cable );
        }

        if( array_has($validated , 'phone' ) and $validated['phone'] != 0  ){
            $phone = Phone::find( $validated['phone'] );
            if( is_null( $phone ) ) return redirect()->back()->withErrors('El servicio de telefonia seleccionado no existe, por favor intentelo de nuevo');
            $package->phone()->associate( $phone );
        }
        $package->save();
        return redirect()->route('package.show' , $package)->withSuccess('Paquete creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        $package->loadMissing(['cable', 'internet', 'phone', 'user']);
        return view('services.package.show', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        $cables = Cable::with('channels')->get();
        $internets = Internet::all();
        $phones = Phone::all();

        return view('services.package.edit' , compact('phones' , 'cables' , 'internets', 'package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'phone' => 'nullable',
            'cable' => 'nullable',
            'internet' => 'nullable',
        ]);
        if( $validated['phone'] == 0 and $validated['cable']  == 0 and $validated['internet'] == 0) return redirect()->back()->withErrors('El paquete tiene que tener al menos un servicio!');
        if( array_has($validated , 'internet' ) and $validated['internet'] != 0  ){
            $internet = Internet::find( $validated['internet'] );
            if( is_null( $internet ) ) return redirect()->back()->withErrors('El servicio de internet seleccionado no existe, por favor intentelo de nuevo');
            $package->internet()->associate( $internet );
        }

        if( array_has($validated , 'cable' ) and $validated['cable'] != 0  ){
            $cable = Cable::find( $validated['cable'] );
            if( is_null( $cable ) ) return redirect()->back()->withErrors('El servicio de cable seleccionado no existe, por favor intentelo de nuevo');
            $package->cable()->associate( $cable );
        }

        if( array_has($validated , 'phone' ) and $validated['phone'] != 0  ){
            $phone = Phone::find( $validated['phone'] );
            if( is_null( $phone ) ) return redirect()->back()->withErrors('El servicio de telefonia seleccionado no existe, por favor intentelo de nuevo');
            $package->phone()->associate( $phone );
        }

        $package->fill( array_only($validated, ['title', 'description'] ) );

        $package->save();
        return redirect()->route('package.show' , $package)->withSuccess('Paquete creado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        //
    }
}
