<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;

use App\CableService as Cable;
class ChannelController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['show', 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channels = Channel::all();
        return view('services.channel.index', compact('channels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = Channel::getDaysCollection();
        return view('services.channel.create', compact('days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:3|max:50',
            'description' => 'required|string|min:10|max:150',
        ]);
        $days = $request->get('programation');
        $collection = collect();
        foreach( $days as $day){
            $collection->push( Channel::parseDay($day) );
        }
        $channel = Channel::create([
            'title' => $validated['title'],
            'description' => $validated['description'],
            'programation' => $collection,
        ]);
        return redirect()->route('channel.show' , $channel)->withSuccess('Servicio creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function show(Channel $channel)
    {
        return view('services.channel.show',compact('channel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        return view('services.channel.edit', compact('channel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Channel $channel)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'programacion' => 'required',
        ]);

        $channel = Channel::create($validated);
        return redirect()->route('channel.show' , $channel)->withSuccess('Servicio editado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Channel $channel)
    {
        $channel->delete();
        return redirect()->route('home')->withSuccess('Servicio borrado con exito');
    }
}
