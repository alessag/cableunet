<?php

namespace App\Http\Controllers;

use App\CableService;
use Illuminate\Http\Request;

use App\Channel;

class CableServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['show', 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cables = CableService::all();
        return view('services.cable.index', compact('cables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $channels = Channel::all();
        return view('services.cable.create', compact('channels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'price' => 'required|numeric|min:1',
            'channels' => 'nullable|array|exists:channels,id',
        ]);
        if( array_has( $validated , 'channels') ){   
            $cable = CableService::Create( array_except( $validated, 'channels' ));
            $cable->channels()->attach( $validated['channels'] );
        } else {
            $cable = CableService::create( $validated );
        }
        $cable->save();
        return redirect()->route('cable.show' , $cable)->withSuccess('Servicio creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CableService  $cable
     * @return \Illuminate\Http\Response
     */
    public function show(CableService $cable)
    {
        return view('services.cable.show',compact('cable'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CableService  $cable
     * @return \Illuminate\Http\Response
     */
    public function edit(CableService $cable)
    {
        $channels = Channel::all();
        return view('services.cable.edit', compact('cable','channels'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CableService  $cable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CableService $cable)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'price' => 'required|numeric|min:1',
            'channels' => 'nullable|array|exists:channels,id',
        ]);
            // TODO assing channels
        if( array_has( $validated , 'channels') ){   
        $cable->fill( array_except( $validated, 'channels' ));
        $cable->channels()->sync( $validated['channels'] );
        } else {
        $cable->fill( $validated );
        }
        $cable->save();
        return redirect()->route('cable.show' , $cable)->withSuccess('Servicio editado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CableService  $cable
     * @return \Illuminate\Http\Response
     */
    public function destroy(CableService $cable)
    {
        $cable->delete();
        return redirect()->route('home')->withSuccess('Servicio borrado con exito');
    }
}
