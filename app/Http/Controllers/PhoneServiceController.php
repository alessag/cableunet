<?php

namespace App\Http\Controllers;

use App\PhoneService;
use Illuminate\Http\Request;

class PhoneServiceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['show', 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phones = PhoneService::all();
        return view('services.phone.index', compact('phones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.phone.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'price' => 'required|numeric|min:1',
            'minutes' => 'required|numeric|min:1',
        ]);

        $phone = PhoneService::create($validated);
        return redirect()->route('phone.show' , $phone)->withSuccess('Servicio creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PhoneService  $phone
     * @return \Illuminate\Http\Response
     */
    public function show(PhoneService $phone)
    {
        return view('services.phone.show',compact('phone'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PhoneService  $phone
     * @return \Illuminate\Http\Response
     */
    public function edit(PhoneService $phone)
    {
        return view('services.phone.edit', compact('phone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PhoneService  $phone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhoneService $phone)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:10|max:50',
            'description' => 'required|string|min:10|max:150',
            'price' => 'required|numeric|min:1',
            'minutes' => 'required|numeric|min:1',
        ]);

        $phone->fill($validated);
        $phone->save();
        return redirect()->route('phone.show' , $phone)->withSuccess('Servicio editado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PhoneService  $phone
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhoneService $phone)
    {
        $phone->delete();
        return redirect()->route('home')->withSuccess('Servicio borrado con exito.');
    }
}
