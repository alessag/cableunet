<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Package;

class InternetService extends Model
{
    /**
     * @var array
     */
    protected $guarded =[];

    public function packages(){
        return $this->hasMany(Package::class);
    }
}
