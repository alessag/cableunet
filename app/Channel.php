<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use CableService as Cable;
class Channel extends Model
{
    /**
     * @var array
     */
    protected $guarded =[];

    public function cables(){
        return $this->belongsToMany(Cable::class);
    }

    public static function getDaysCollection(){
        return collect([
            ['es' => 'lunes' , 'en' => 'monday'],
            ['es' => 'martes' , 'en' => 'tuesday'],
            ['es' => 'miercoles' , 'en' => 'wednesday'],
            ['es' => 'jueves' , 'en' => 'thursday'],
            ['es' => 'viernes' , 'en' => 'friday'],
            ['es' => 'sabado' , 'en' => 'saturday'],
            ['es' => 'domingo' , 'en' => 'sunday'],
        ]);
    }

    public static function parseDay( $day){
        if( is_null($day) or !str_contains( $day , ['/' , ';']) ) return '';
        
        $result = collect();
        $shows = explode(';' , $day ); 
        foreach($shows as $show){
            $result->push( collect(explode('/' , $show) ) );
        }
        return json_encode($result);
    }
    
    public function getProgramationCollection(){
        $collection =  collect(json_decode( $this->programation ) );
        $collection = $collection->map(function($value, $key){
            $result = collect( json_decode( $value ) );
            $result->map('collect');
            return $result;
        });
        // $collection = $collection->map('collect' );
        return $collection;
    }
}
