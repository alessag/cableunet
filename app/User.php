<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Role;
use App\Package;
use App\Invoice;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The role associate with this user
     * 
     * @return App\Role
     */
    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }

    public function invoices(){
        return $this->hasMany(Invoice::class);
    }
    
    public function getThisMonthInvoice(){
        
        if( ! $this->hasPackage() ) return null;

        $invoices = $this->invoices; 
        if( is_null( $invoices )) return $this->createCurrentInvoice();

        $invoices = $invoices->filter(function ($value, $key){
            return  $value->updated_at->month == now()->month ;
        });

        if ( $invoices->isEmpty() ) return $this->createCurrentInvoice();

        $maxPrice = $invoices->max->price;

        return $invoices->sortBy('updated_at')->firstWhere('price', $maxPrice);
    }

    public function createCurrentInvoice( $user = null ){
        if( is_null( $user ) ) $user = auth()->user();
        if( ! $user->isPackageApproved() ) return null;
        $invoice = Invoice::create([
                'user_id' => $user->id,
                'package_name' => $user->package->title,
                'package_description' => $user->package->description,
                'total' => $user->package->getTotalPrice(),
        ]);
        return $invoice;
    }

    /**
     * Check is the user is Admin (role id = 0)
     * @return bool
     */
    public function isAdmin(){ return ($this->role->id == 0); }

    public function hasPackage(){ return ! is_null($this->package);  }

    public function isPackageApproved(){
        if( ! $this->hasPackage() ) return false;
        return $this->package_active;
    }

    public function approvePackage(){
        $this->package_active = true;
    }

    public function pendingPackage(){
        $this->package_active = false;
    }
}
