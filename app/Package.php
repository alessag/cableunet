<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\CableService as Cable;
use App\InternetService as Internet;
use App\PhoneService as Phone;
use App\User;

class Package extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    public function cable(){
        return $this->belongsTo(Cable::class, 'cable_service_id');
    }

    public function internet(){
        return $this->belongsTo(Internet::class, 'internet_service_id');
    }

    public function phone(){
        return $this->belongsTo(Phone::class, 'phone_service_id');
    }

    public function user(){
        return $this->hasMany(User::class);
    }

    public function getTotalPrice(){
        $total = 0;
        if( $this->hasCable() ) $total+= $this->cable->price;
        if( $this->hasInternet() ) $total+= $this->internet->price;
        if( $this->hasPhone() ) $total+= $this->phone->price;
        return $total;
    }
    public function hasCable() : bool {return !is_null($this->cable); }

    public function hasInternet() : bool {return !is_null($this->internet); }
    
    public function hasPhone() : bool {return !is_null($this->phone); }
}
