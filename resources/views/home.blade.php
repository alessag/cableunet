@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if( auth()->user()->hasPackage() && !auth()->user()->isPackageApproved() )
        <div class="col-md-8 alert alert-primary p-3">
            <h3 class="text-center"> Tu paquete aun no ha sido aprobado! </h3>
        </div>
        @endif
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard - Bienvenido</div>
                <div class="card-body">
                    @if( auth()->user()->hasPackage() && auth()->user()->isPackageApproved() )
                    <div class="p-3">
                        <h5 class="text-left"> <a href="{{ route('package.show', auth()->user()->package ) }}"> Este es tu paquete actual.</a> </h5>
                    </div>
                    @endif
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a href="{{ route('package.index') }}">Ver los paquetes</a></li>
                        <li class="list-group-item"><a href="{{ route('channel.index') }}">Ver los canales</a></li>
                        <li class="list-group-item"><a href="{{ route('internet.index') }}">Ver los servicios de internet</a></li>
                        <li class="list-group-item"><a href="{{ route('phone.index') }}">Ver los servicios de telefonia</a></li>
                        <li class="list-group-item"><a href="{{ route('cable.index') }}">Ver los servicios de cable</a></li>

                        @if( auth()->user()->hasPackage() )
                        <li class="list-group-item">
                             <a href="{{ route('invoice.show', auth()->user()->getThisMonthInvoice()) }}">Ver la factura de este mes</a> 
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
