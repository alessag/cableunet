@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($invoices as $invoice)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            @if (  ! auth()->user()->isAdmin() ) <h1> Tus facturas </h1> @else  <h1>Facturas disponibles </h1>@endif
        </div>
        @endif
        
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('invoice.show', $invoice) }}"> Factura numero  #{{$invoice->id}}</a> del mes:<strong>{{ $invoice->updated_at->format(' m/Y') }} </strong>  @if (  auth()->user()->isAdmin() ) de <a href="{{route('user.show', $invoice->user )}}" class="btn" > {{$invoice->user->name}} </a>@endif</div>
            </div>
        </div>
        @empty
        <div class="col-md-8 text-center">
            <h1>Aun no tienes facturas disponibles!</h1>
        </div>
        @endforelse
    </div>
</div>
@endsection
