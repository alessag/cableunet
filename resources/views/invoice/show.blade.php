@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                        <h4>Beneficiario: {{$invoice->user->name}}</h4>
                    </div>
                    <div class="float-right">
                        <h4>Numero de factura #{{$invoice->id}} </h4>
                    </div>
                </div>

                <div class="card-body">
                    <h5 class="p-2 my-3">Detalles: <span class="float-right">Factura del mes: <strong> {{ $invoice->updated_at->format(' m/Y') }} </strong></span> </h5>
                    
                    <br>
                    <div class="">
                        <div class="float-left">
                            <h6>Nombre del paquete: {{$invoice->package_name}} </h6>
                            <p class="my-3">
                                {{$invoice->package_description}}
                            </p>
                        </div>
                        <div class="float-right">
                            <h6> Costo total mensual <strong> {{$invoice->total}} </strong> (BsS) </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
