<div class="row my-3 ">
    <div class="col-sm-7 mx-auto">
        <div class="alert alert-success normal-notification hide" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        <h5 class="text-success alert-heading"><i class="fa fa-check-circle-o"></i> {{ session('success') }}</h5>
    </div>
</div>