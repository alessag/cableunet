<div class="row my-3 ">
    <div class="col-sm-7 mx-auto">
        <div class="alert alert-danger normal-notification hide" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading text-left">Hay algunos errores!</h3>
            
            @foreach ($errors->all() as $error)
            
            @if ($loop->first)
                <ul>
            @endif            
                <li> <h4>{{$error}}</h4></li>
            @if ($loop->last)
            </ul>
            @endif
            
            @endforeach
        </div>
    </div>
</div>