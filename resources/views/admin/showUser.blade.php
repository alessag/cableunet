@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                        <h3>{{$user->name}}</h3>
                    </div>
                </div>
                <div class="card-body">
                    @forelse ($user->invoices as $invoice)
                    @if ($loop->first)
                    <div class="col-md-12 text-center">
                        @if ( auth()->user() == $user ) <h1>Tus facturas </h1> @else <h1>Las facturas de este usuario </h1> @endif
                    </div>
                    @endif
                    
                    <div class="col-md-12 mt-2">
                        <div class="card">
                            <div class="card-header"> <a class="btn" href="{{ route('invoice.show', $invoice) }}"> Factura numero  #{{$invoice->id}}</a> del mes:<strong>{{ $invoice->updated_at->format(' m/Y') }} </strong> </div>
                        </div>
                    </div>
                    @empty
                    <div class="col-md-8 text-center">
                        <h1>Este usuario aun no tienes facturas disponibles!</h1>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
