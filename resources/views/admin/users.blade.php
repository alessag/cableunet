@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($users as $user)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            <h1>Usuarios Registrados</h1>
        </div>
        @endif
        
        <div>
            <div class="card">
                <div class="card-header">
                    <a class="btn float-left" href="{{ route('user.show', $user) }}"> {{$user->name}} </a>
                    @if( $user->hasPackage() && !$user->isPackageApproved() )
                    <form action="{{ route('user.ok') }}" method="POST">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <button type="submit" class="float-right btn btn-outline-primary">Aprobar</button>
                    </form>
                    @endif
                </div>
                <div class="card-body">
                    @if ( $user->hasPackage() )
                        Suscrito a <a href="{{route('package.show', $user->package)}}">{{$user->package->title}}</a>
                        <div class="float-right">
                            @if ( $user->isPackageApproved() )
                                <span class="text-success">Aprobado.</span>
                            @else
                                <span class="text-danger">Esta subscripcion aun no ha sido aprobada.</span>
                            @endif
                        </div>
                    @else
                        <h3 class="text-center">Este usuario no esta suscrito a ningun paquete!</h3>
                    @endif
                <div>
                </div>
            </div>
        </div>
        @empty
        <div class="col-md-8 text-center">
            <h1>No hay usuarios registrados!</h1>
        </div>
        @endforelse
    </div>
</div>
@endsection
