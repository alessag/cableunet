@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Panel - Bienvenido</div>
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-6">
                            <h3>Servicios</h3>
                            <div>
                                <ul class="list-group list-group-flush">
                                <li class="list-group-item"><a class="btn" href="{{ route('phone.index') }} ">Teléfono</a></li>
                                <li class="list-group-item"><a class="btn" href="{{ route('internet.index') }} ">Internet</a></li>
                                <li class="list-group-item"><a class="btn" href="{{ route('channel.index') }} ">Canales</a></li>
                                <li class="list-group-item"><a class="btn" href="{{ route('cable.index') }} ">Cable</a></li>
                                <li class="list-group-item"><a class="btn" href="{{ route('package.index') }} ">Paquetes</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6">
                            <h3>Creación de Servicios</h3>
                            <div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a class="btn" href="{{ route('phone.create') }} ">Teléfono</a></li>
                                    <li class="list-group-item"><a class="btn" href="{{ route('internet.create') }} ">Internet</a></li>
                                    <li class="list-group-item"><a class="btn" href="{{ route('channel.create') }} ">Canal</a></li>
                                    <li class="list-group-item"><a class="btn" href="{{ route('cable.create') }} ">Cable</a></li>
                                    <li class="list-group-item"><a class="btn" href="{{ route('package.create') }} ">Paquete</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                   <div class="col-12">
                        <h3>Usuarios</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item" ><a class="btn" href="{{ route('user.index') }} ">Usuarios</a></li>
                        </ul>
                    </div>
                    <div class="col-12">
                        <h3>Programación</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item" ><a class="btn" href="">Programación</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
