<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body>
    <div id="app">
        @include('layouts.navbar')
        <main class="py-4">
            @includeWhen($errors->any(), 'shared.errors')
            @includeWhen( session()->has('success'), 'shared.success')
            @yield('content')
        </main>
    </div>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script>
    jQuery(document).ready(function() {
    /**
    * Show the notification after the page is loaded then hide after 5 seconds with animations 
    */
    $('.normal-notification').delay(500).slideDown().delay(5000).slideUp();

    @yield('readyFunction')

    });
    </script>
    <script></script>
</body>
</html>
