@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($channels as $channel)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            <h1>Paquetes Disponibles</h1>
        </div>
        @endif
        
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('channel.show', $channel) }}"> {{$channel->title}} </a></div>
                <div class="card-body">
                <div>
                    <p>Descripción: {{$channel->description}}</p>
                </div>
                </div>
            </div>
        </div>
        @empty
        <div class="col-md-8 text-center">
            <h1>Aun no hay canales disponibles</h1>
        </div>
        @endforelse
    </div>
</div>
@endsection
