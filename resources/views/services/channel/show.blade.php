@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
            <h1>Canal Disponible</h1>
        </div>
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('channel.show', $channel) }}"> {{$channel->title}} </a></div>
                <div class="card-body">
                <div>
                    <p>Descripción: {{$channel->description}}</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
