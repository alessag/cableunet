@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar nuevo servicio</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('channel.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Descripcion</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" required>

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="" class="col-md-12 col-form-label text-md-center">Programacion </label>
                            <div class="col-md-12 p-3 mx-auto">
                                <p class="text-left p-3">
                                    Por favor indice la programacion de este canal para cada dia de la semana en el siguiente formato: <br>
                                    "Nombre del programa" <strong>/</strong> "Horario de inicio" <strong>/</strong> "Horario de finalizacion" ;
                                </p>
                                <ul>
                                    <li>
                                        Separa cada programa con un ";"
                                    </li>
                                    <li>
                                        El formato del horario es de 00:00 hasta 23:55
                                    </li>
                                    <li>
                                        Evite conflicto de horarios entre programas
                                    </li>
                                    <li>
                                        Horas no especificadas sera sustituido por "Informacion no provista por el proveedor del canal"
                                    </li>
                                    <li>
                                        De no cumplir el formato especificado sus horas no seran guardadas correctamente
                                    </li>
                                </ul>
                            </div>
                            @foreach ($days as $day)
                            <div class="col-md-4 uppercase">
                                <label class="text-center text-capitalize" for="{{ $day['en']}}">{{ $day['es'] }}</label>
                                <textarea class="form-control{{ $errors->has('programation') ? ' is-invalid' : '' }}" 
                                    name="programation[{{ $day['en']}}]" id="{{ $day['en']}}" cols="30" rows="5"
                                    placeholder="Ejemplo del formato: Noticiero / 12:00 / 15:30 ;"></textarea>
                            </div>
                            @endforeach
                            
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
