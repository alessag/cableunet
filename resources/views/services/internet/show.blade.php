@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
            <h1>Paquete De Internet Disponible</h1>
        </div>
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('internet.show', $internet) }}"> {{$internet->title}} </a></div>
                <div class="card-body">
                <div>
                    <p>Descripción: {{$internet->description}}</p>
                    <p>Velocidad: {{$internet->speed}} mb</p>
                    <p>Precio: {{$internet->price}} Bs.</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
