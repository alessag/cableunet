@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
            <h1>Servicio de Cable Disponibles</h1>
        </div>
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('cable.show', $cable) }}"> {{$cable->title}} </a></div>
                <div class="card-body">
                <div>
                    <p>Descripción: {{$cable->description}}</p>
                    <p>Precio: {{$cable->price}} Bs.</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
