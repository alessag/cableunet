@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar nuevo servicio</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('cable.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $cable->title }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Descripcion</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $cable->description }}" required>

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">Precio (BsS) </label>

                            <div class="col-md-6">
                                <input id="price" type="number" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{ $cable->price }}" required>

                                @if ($errors->has('price'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="channels" class="col-md-4 col-form-label text-md-right">Canales </label>
                            <div class="col-md-6">
                        @forelse ($channels as $channel)
                            @if ($loop->first)
                            <select id="channels" class="form-control{{ $errors->has('channels') ? ' is-invalid' : '' }}" name="channels[]" multiple>
                            @endif
                            <option value="{{$channel->id}}"  @unless(is_null( $cable->channels )) {{ $cable->channels->contains($channel) ? 'selected' : '' }} @endunless >{{$channel->title}}</option>
                            @if ($loop->last)
                            </select>
                            @if ($errors->has('channels'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('channels') }}</strong>
                                </span>
                            @endif                         
                            @endif
                            
                        @empty
                            <h6>No hay canales disponible! Puedes crear un canal <a href="{{route('channel.create')}}">aqui</a>.</h6>
                        @endforelse
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
