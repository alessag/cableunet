@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($phones as $phone)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            <h1>Paquete De Teléfono Disponibles</h1>
        </div>
        @endif
        
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('phone.show', $phone) }}"> {{$phone->title}} </a></div>
                <div class="card-body">
                <div>
                    <p>Descripción: {{$phone->description}}</p>
                    <p>Minutos: {{$phone->minutes}} min</p>
                    <p>Precio: {{$phone->price}} Bs.</p>
                </div>
                </div>
            </div>
        </div>
        @empty
        <div class="col-md-8 text-center">
            <h1>Aun no hay paquetes disponibles!</h1>
        </div>
        @endforelse
    </div>
</div>
@endsection
