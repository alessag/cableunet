@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                        <h3>{{$package->title}}</h3>
                    </div>
                    @if ( auth()->user()->isAdmin())                        
                    <div class=" d-inline-flex float-right ">
                        <a href="{{route('package.edit', $package)}}" class="btn btn-outline-success btn-md mr-2">Editar</a>
                        <form action="{{route('package.destroy', $package)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" href="{{route('package.edit', $package)}}" class="btn btn-danger btn-md">Borrar</button>
                        </form>
                    </div>
                    @endif
                </div>
                <div class="card-body">
                    <h5 class="float-right">
                        Precio total del paquete: <strong>{{ $package->getTotalPrice() }}</strong> (BsS)
                    </h5>
                    <h4>Descripcion: </h4>
                    <p>
                        {{$package->description}}
                    </p>
                    <hr>
                    <h4>Incluye:</h4>
                    @if ( $package->hasCable() )
                    <hr>
                        <h5 class="float-right">
                            Precio total del servicio: <strong>{{ $package->cable->price }}</strong> (BsS)
                        </h5>
                        <h5>
                            Nombre del servicio <a href="{{route('cable.show', $package->cable)}}" class="btn text-primary"><strong> {{$package->cable->title}} </strong> </a>
                        </h5>
                        <p>
                            {{$package->cable->description}}
                        </p>
                        @forelse ($package->cable->channels as $channel)
                            @if ($loop->first)
                                <ul>                                    
                            @endif
                            <li>
                                <h6><a href="{{route('channel.show', $channel)}}" class="btn">{{$package->title}}</a></h6>
                                <p>
                                    {{$channel->description}}    
                                </p> 
                            </li>
                            @if ($loop->last)
                                </ul>
                            @endif                         
                        @empty
                            <h6 class="text-danger">Este servicio de cable no tiene canales disponibles!</h6>
                        @endforelse
                    @endif
                    @if ( $package->hasInternet() )
                    <hr>
                        <h5 class="float-right">
                            Precio total del servicio: <strong>{{ $package->internet->price }}</strong> (BsS)
                        </h5>
                        <h5>
                            Nombre del servicio <a href="{{route('internet.show', $package->internet)}}" class="btn text-primary"><strong> {{$package->internet->title}} </strong> </a>
                        </h5>
                        <p>
                            {{$package->internet->description}}
                        </p>
                        <h5 class="">{{$package->internet->speed}} (Mbs) !</h5>
                    @endif
                    @if ( $package->hasPhone() )
                    <hr>
                        <h5 class="float-right">
                            Precio total del servicio: <strong>{{ $package->phone->price }}</strong> (BsS)
                        </h5>
                        <h5>
                            Nombre del servicio <a href="{{route('phone.show', $package->phone)}}" class="btn text-primary"><strong> {{$package->phone->title}} </strong> </a>
                        </h5>
                        <p>
                            {{$package->phone->description}}
                        </p>
                        <h5 class="">{{$package->phone->minutes}} Minutos! </h5>
                    @endif
                    <hr>
                    @unless( auth()->user()->isAdmin() )
                    <div class="text-center">
                        <form action="{{route('package.add')}}" method="POST">
                        @csrf
                        <input type="hidden" name="package_id" value="{{$package->id}}">
                        <button class="text-center btn btn-large btn-primary">Suscribirme a este paquete!</button>
                        </form>
                    </div>
                    @endunless
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
