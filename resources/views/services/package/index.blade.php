@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($packages as $package)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            <h1>Paquetes Disponibles</h1>
        </div>
        @endif
        
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('package.show', $package) }}"> {{$package->title}} </a></div>
                <div class="card-body">
                <div>
                    <h5>Descripcion:</h5>
                    <p>
                        {{$package->description}}
                    </p>
                </div>
                Incluye: 
                <ul>
                    @if ( $package->hasCable() )
                    <li>
                        <a href="{{ route('cable.show', $package->cable) }}" class="btn"> {{$package->cable->title}} </a>
                    </li>    
                    @endif
                    @if ( $package->hasPhone() )
                    <li>
                        <a href="{{ route('phone.show', $package->phone) }}" class="btn"> {{$package->phone->title}} </a>
                    </li>    
                    @endif
                    @if ( $package->hasInternet() )
                    <li>
                        <a href="{{ route('internet.show', $package->internet) }}" class="btn"> {{$package->internet->title}} </a>
                    </li>    
                    @endif
                </ul>
                </div>
            </div>
        </div>
        @empty
        <div class="col-md-8 text-center">
            <h1>Aun no hay paquetes disponibles!</h1>
        </div>
        @endforelse
    </div>
</div>
@endsection
