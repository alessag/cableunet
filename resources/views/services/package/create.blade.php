@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crea un nuevo paquete de Servicios</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('package.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Descripcion</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" required>

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="cable" class="col-md-4 col-form-label text-md-right">Servicios de cable </label>
                            <div class="col-md-6">
                        @forelse ($cables as $cable)
                            @if ($loop->first)
                            <select id="cable" class="form-control{{ $errors->has('cable') ? ' is-invalid' : '' }}" name="cable" >
                            <option value="0" >Seleccione un servicio de cable</option>
                            @endif
                            <option value="{{$cable->id}}" {{ old('cable') == $cable->id ? ' selected ' : '' }} 
                                    data-title="{{$cable->title}}" data-price="{{$cable->price}}" data-description="{{$cable->description}}" 
                                    data-channels="{{$cable->channels}}" >
                                    {{$cable->title}} </option>
                            @if ($loop->last)
                            </select>                       
                            @endif
                            
                        @empty
                            <h6>No hay servicios de cable disponible! Puedes crear uno <a href="{{route('cable.create')}}">aqui</a>.</h6>
                        @endforelse
                            </div>
                            <div id="cable-wrapper" class="col-md-6 mx-auto my-2 p-3 cable" style="display:none">
                                <div  >
                                    <div class="float-left">
                                        <h5 id="cable-title" class="font-weight-bold"> </h5>
                                        <h6 id="cable-channels" class=""></h6>
                                        <p id="cable-description"></p>
                                    </div>
                                    <div class="float-right">
                                        <h5 class=" text-success"> Seleccionado </h5>
                                        <span id="cable-price" class=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Servicios de telefonia </label>
                            <div class="col-md-6">
                        @forelse ($phones as $phone)
                            @if ($loop->first)
                            <select id="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" >
                            <option value="0" >Seleccione un servicio de telefonia</option>
                            @endif
                            <option value="{{$phone->id}}" {{ old('phone') == $phone->id ? ' selected ' : '' }} 
                                    data-title="{{$phone->title}}" data-price="{{$phone->price}}" data-description="{{$phone->description}}" data-minutes="{{$phone->minutes}}">
                                    {{$phone->title}} - {{ $phone->minutes}} Minutos </option>
                            @if ($loop->last)
                            </select>                       
                            @endif
                            
                        @empty
                            <h6>No hay servicios de telefonia disponible! Puedes crear uno <a href="{{route('phone.create')}}">aqui</a>.</h6>
                        @endforelse
                            </div>
                            <div id="phone-wrapper" class="col-md-6 mx-auto my-2 p-3 phone" style="display: none;">
                                <div >
                                    <div class="float-left">
                                        <h5 id="phone-title" class="font-weight-bold"> </h5>
                                        <h6 id="phone-minutes" class=""></h6>
                                        <p id="phone-description"></p>
                                    </div>
                                    <div class="float-right">
                                        <h5 class=" text-success"> Seleccionado </h5>
                                        <span id="phone-price" class=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="internet" class="col-md-4 col-form-label text-md-right">Servicios de Internet </label>
                            <div class="col-md-6">
                        @forelse ($internets as $internet)
                            @if ($loop->first)
                            <select id="internet" class="form-control{{ $errors->has('internet') ? ' is-invalid' : '' }}" name="internet" >
                            <option value="0" >Seleccione un servicio de Internet</option>
                            @endif
                            <option value="{{$internet->id}}" {{ old('internet') == $internet->id ? ' selected ' : '' }} 
                                    data-title="{{$internet->title}}" data-price="{{$internet->price}}" data-description="{{$internet->description}}" data-speed="{{$internet->speed}}">
                                    {{$internet->title}} - {{ $internet->speed}} Mbs </option>
                            @if ($loop->last)
                            </select>                       
                            @endif
                            
                        @empty
                            <h6>No hay servicios de Internet disponible! Puedes crear uno <a href="{{route('internet.create')}}">aqui</a>.</h6>
                        @endforelse
                            </div>
                            <div id="internet-wrapper" class="col-md-6 mx-auto my-2 p-3 internet" style="display: none;">
                                <div >
                                    <div class="float-left">
                                        <h5 id="internet-title" class="font-weight-bold"> </h5>
                                        <h6 id="internet-speed" class=""></h6>
                                        <p id="internet-description"></p>
                                    </div>
                                    <div class="float-right">
                                        <h5 class=" text-success"> Seleccionado </h5>
                                        <span id="internet-price" class=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('readyFunction')
//Function to display the cable features when a new cable service is selected
function changeCable(){
    $('#cable-title').text( $('#cable').find('option:selected').data('title') )
    $('#cable-price').text( $('#cable').find('option:selected').data('price') + ' (BsS) ' )
    var channels = $('#cable').find('option:selected').data('channels');
    var texto = '';
    for(var ch of channels){
        texto += ' -' + ch.title + " <br> "
    }
    console.log(texto)
    $('#cable-channels').html( texto )
    $('#cable-description').text( $('#cable').find('option:selected').data('description') )
    $('#cable-wrapper').show()
}
// Check when the page is loaded
if ($('#cable').val() !=0 && $('#cable').val() != undefined  ){
    changeCable()
}
//Change when the new cable service is selected
$('#cable').change(function(){
    if( this.value == 0){
        $('#cable-wrapper').hide()
    } else {
        changeCable()
    }
});

function changePhone(){
    $('#phone-title').text( $('#phone').find('option:selected').data('title') )
    $('#phone-price').text( $('#phone').find('option:selected').data('price') + ' (BsS) ' )
    $('#phone-minutes').text( $('#phone').find('option:selected').data('minutes') + ' Minutos ' )
    $('#phone-description').text( $('#phone').find('option:selected').data('description') )
    $('#phone-wrapper').show()
}

if ($('#phone').val() !=0 && $('#phone').val() != undefined ){
    changePhone()
}

$('#phone').change(function(){
    if( this.value == 0){
        $('#phone-wrapper').hide()
    } else {
        changePhone()
    }
});

function changeInternet(){
    $('#internet-title').text( $('#internet').find('option:selected').data('title') )
    $('#internet-price').text( $('#internet').find('option:selected').data('price') + ' (BsS) ' )
    $('#internet-speed').text( $('#internet').find('option:selected').data('speed') + ' Mbs ' )
    $('#internet-description').text( $('#internet').find('option:selected').data('description') )
    $('#internet-wrapper').show()
}

if ($('#internet').val() !=0 && $('#internet').val() != undefined ){
    changeInternet()
}

$('#internet').change(function(){
    if( this.value == 0){
        $('#internet-wrapper').hide()
    } else {
        changeInternet()
    }
});
@endsection